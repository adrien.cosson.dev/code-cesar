<?php
/** TODO
 * Trouver pourquoi il y a des espaces dans les textarea
 *  Ajouter les Majuscules
 *  valeur par défaut dans l'input type number
*/

$alphabet = "abcdefghijklmnopqrstuvwxyz";
$decalage = [];

if (isset($_POST['inputText']) && $_POST['inputText'] !== '' && isset($_POST['action'])) {
    $message = $_POST['inputText'];
    // Décoder un message
    if ($_POST['action'] == 'decode') {
        // Décoder sans connaitre le décalage
        if ($_POST['decalage'] == '') {
            $decalage = calcul_auto_decal($message, $alphabet);
            $finalMessage = decodage($message, $decalage, $alphabet);
        }
        // Décoder en connaisant le décalage
        else {
            array_push($decalage, $_POST['decalage']);
            $finalMessage = decodage($message, $decalage, $alphabet);
        }
    }
    else {
        // Coder un message
        if ($_POST['decalage'] !== '') {
            $decalage = $_POST['decalage'];
            $finalMessage = codage($message, $decalage);
        }
    }
}

//$message = "R'thi jc vpgh, xa gtcigt spch jc rpué, ti eadju...";
//$decalage = calcul_auto_decal($message, $alphabet);
//print_r($decalage);
//$finalMessage = decodage($message, $decalage, $alphabet);

function codage ($m, $d) {
$alphabet = "abcdefghijklmnopqrstuvwxyz";
$encoded="";
$m = strtolower($m);
for ($i=0 ; $i<strlen($m) ; $i++) {
    if (strpos($alphabet, $m[$i]) === false) {
        $encoded .= $m[$i];
    } else {
        $newLetter = ($d + strpos($alphabet, $m[$i])) % 26;
        $encoded .= $alphabet[$newLetter];
    }
}
    return $encoded;
}

function decodage ($m, $d, $alphabet) {
    $m = strtolower($m);
    $decoded="";
    for ($j=0 ; $j<count($d) ; $j++) {

        for ($i=0 ; $i<strlen($m) ; $i++) {
            if (strpos($alphabet, $m[$i]) === false) {

                $decoded .= $m[$i];
            } else {
                $newLetter = (-$d[$j] + strpos($alphabet, $m[$i])) % 26;
                $decoded .= $alphabet[$newLetter];
            }
        }
    }
    return($decoded);
}

function feq_lettres($m, $letter, $alphabet) {
    $freq = null;
    for ($i=0 ; $i<strlen($m) ; $i++) {
        if ( $m[$i] == $alphabet[$letter]) {
            $freq++;
        }
    }
    return $freq;
}

function calcul_auto_decal($m, $alphabet) {
    $m = strtolower($m);
    $freq = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    for ($i=0 ; $i<strlen($m) ; $i++) {
        if (strpos($alphabet, $m[$i]) !== false) {
            $freq[strpos($alphabet, $m[$i])]++;
        }
    }

// Trouver la lettre la plus utilisée et sa position
    $maxChar = max($freq);
    $maxCharPos = [];
    for ($i=0 ; $i<count($freq) ; $i++) {

        if ($maxChar == $freq[$i]) {
            array_push($maxCharPos, $i);
        }
    }
// Trouver le décalage entre la lettre 'e' et $maxCharPos
    $decalage = [];
    for ($i=0 ; $i<count($maxCharPos) ; $i++) {
        array_push($decalage, $maxCharPos[$i] - 4);
    }
    return $decalage;
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Code de César</title>
    <form method="POST">
        <label for="inputText">Text à déchiffrer</label>
        <textarea name="inputText" cols="30" rows="10"><?php
            if (isset($message) && $message != " ") {
                echo $message;
            } ?>
        </textarea>

        <label for="outputText">Text déchiffré</label>
            <textarea name="outputText" cols="30" rows="10"><?php
                if (isset($finalMessage) && $finalMessage != " ") {
                    echo $finalMessage;
                } ?></textarea>'
        <div>
            <input type="radio" id="code" name="action" value="code">
            <label for="code">Coder</label>
        </div>

        <div>
            <input type="radio" id="decode" name="action" value="decode">
            <label for="decode">Décoder</label>
        </div>

        <input type="number" name="decalage">
        <input type="submit" value="Déchiffrer">
<!--        <p>Décalage de --><?php //echo $decalage ?><!--</p>-->
    </form>
</head>
<body>

</body>
</html>


